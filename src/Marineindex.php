<?php

namespace Seaport\Database;

class Marineindex {


      public static function database(){
            $jsonForm = dirname(__FILE__).'/j.json';
            $formCollection = [];
    
            if(file_exists($jsonForm)){
                $data = file_get_contents($jsonForm);
                $formCollection = json_decode($data ,true);
            }
    
            return $formCollection;
        }

      public static function index(){
            return self::database();
      }

      public static function search_by_country_code($en){
            $data = self::database();
            $matches = [];
        
            foreach ($data as $index => $car) {
                if ($car["Country Code"] == $en) {
                    $matches[] = $car;
                }
            }
        
            return $matches;
        }
}